import { getHistoryMessage } from '@/net/send-message'
import store from '@/store/index'

export default {

  fetchMessages: (state, data) => {
    const { roomId, type, messageId } = data
    console.log('请求数据', { roomId, type, messageId })
    // 首次请求消息ID为空
    if (!messageId) {
      if (state.cacheMessages) {
        state.messages = [...state.cacheMessages.slice(state.cacheMessages.length - 20, state.cacheMessages.length)]
        if (state.cacheMessages.length < 20) {
          store.commit('setMessageLoaded', true)
        }
        return
      }
      getHistoryMessage({ roomId, type, messageId })
      return
    }
    const index = state.cacheMessages.findIndex(x => x._id === messageId)
    // 向上滚动
    if (type.toUpperCase() === 'TOP') {
      if (state.messageLoaded) {
        return
      }
      console.log(type, index)
      if (index < 20) {
        if (state.cacheMessageLoaded) {
          console.log('cacheMessageLoaded')
          state.messages = [...state.cacheMessages.slice(0, index), ...state.messages]
          store.commit('cutMessages', { start: false })
          store.commit('setMessageLoaded', true)
        } else {
          getHistoryMessage({ roomId, type, messageId })
        }
      } else {
        console.log('数量20条')
        state.messages = [...state.cacheMessages.slice(index - 20, index), ...state.messages]
        store.commit('cutMessages', { start: false })
      }
    } else {
      if (state.newMessageLoaded) {
        return
      }
      console.log('新消息加载完了吗?', state.newMessageLoaded)

      if (state.cacheMessages.length - index < 20) {
        if (state.cacheNewMessageLoaded) {
          state.messages = [...state.messages, ...state.cacheMessages.slice(index + 1, state.cacheMessages.length)]
          store.commit('cutMessages', { start: true })
          store.commit('setNewMessageLoaded', true)
        } else {
          getHistoryMessage({ roomId, type, messageId })
        }
      } else {
        console.log('数量向下20条')
        state.messages = [...state.messages, ...state.cacheMessages.slice(index + 1, index + 20)]
        store.commit('cutMessages', { start: true })
      }
    }
  },

  pushMessage: (state, message) => {
    const checkIndex = state.cacheMessages.findIndex(r => r._id === message._id)
    if (checkIndex !== -1) {
      state.cacheMessages[checkIndex] = message
    } else {
      state.cacheMessages.push(message)
    }
    if (state.newMessageLoaded) {
      store.commit('pushLoadMessage', message)
    }
  },

  pushLoadMessage: (state, message) => {
    const checkIndex = state.messages.findIndex(r => r._id === message._id)
    if (checkIndex !== -1) {
      state.messages[checkIndex] = message
    } else {
      state.messages.push(message)
    }
    store.commit('cutMessages', { start: true })
  },

  clearMessages: (state, type) => {
    if (type === 'cacheMessage') {
      state.cacheMessages = []
    } else if (type === 'message') {
      state.messages = []
    } else {
      state.cacheMessages = []
      state.messages = []
    }
  },

  // 吸收消息
  assimilateMessages: (state, data) => {
    const { loadMessages: messages, type, messageId, roomId } = data
    messages.forEach(x => {
      const cacheIndex = state.cacheMessages.findIndex(r => r._id === x._id)
      if (cacheIndex === -1 && type === 'TOP') {
        state.cacheMessages.unshift(x)
      } else if (cacheIndex === -1 && type === 'DOWN') {
        state.cacheMessages.push(x)
      }
    })
    store.commit('fetchMessages', { roomId, type, messageId })
  },

  // start 为true时截取尾部60条, 即向下划
  cutMessages: (state, { start, index }) => {
    if (index) {
      const start = (index - 30) < 0 ? 0 : index - 30
      const end = (index + 30) > state.cacheMessages.length ? state.cacheMessages.length : index + 30
      state.messages = [...state.cacheMessages.slice(start, end)]
      return
    }
    if (start) {
      if (state.messages.length > 60) {
        store.commit('setMessageLoaded', false)
        state.messages = [...state.messages.slice(state.messages.length - 60, state.messages.length)]
      }
    } else {
      if (state.messages.length > 60) {
        store.commit('setNewMessageLoaded', false)
        store.commit('setCacheNewMessageLoaded', true)
      }
      state.messages = [...state.messages.slice(0, 60)]
    }
  },

  setNewMessageLoaded: (state, searchMessage) => { state.newMessageLoaded = searchMessage },

  setCacheMessageLoaded: (state, cacheMessageLoaded) => { state.cacheMessageLoaded = cacheMessageLoaded },

  setCacheNewMessageLoaded: (state, cacheNewMessageLoaded) => { state.cacheNewMessageLoaded = cacheNewMessageLoaded }

}
